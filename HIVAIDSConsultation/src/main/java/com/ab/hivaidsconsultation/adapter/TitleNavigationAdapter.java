package com.ab.hivaidsconsultation.adapter;

import android.app.Activity;
import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ab.hivaidsconsultation.activity.R;
import com.ab.hivaidsconsultation.model.SpinnerNavigationItem;

import java.util.ArrayList;

/**
 * Created by biniam on 3/26/14.
 */
public class TitleNavigationAdapter extends BaseAdapter {

    private ImageView imageIcon;
    private TextView textTitle;
    private ArrayList<SpinnerNavigationItem> spinnerNavigationItems;
    private Context context;

    public TitleNavigationAdapter(Context context, ArrayList<SpinnerNavigationItem> spinnerNavigationItems) {
        this.context = context;
        this.spinnerNavigationItems = spinnerNavigationItems;
    }

    @Override
    public int getCount() {
        return spinnerNavigationItems.size();
    }

    @Override
    public Object getItem(int index) {
        return spinnerNavigationItems.get(index);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(null == convertView) {

            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            convertView = layoutInflater.inflate(R.layout.list_item_title_navigation, null);
        }

        imageIcon = (ImageView) convertView.findViewById(R.id.imageIcon);

        textTitle = (TextView) convertView.findViewById(R.id.textTitle);

        imageIcon.setImageResource(spinnerNavigationItems.get(position).getIcon());
        imageIcon.setVisibility(View.GONE);

        textTitle.setText(spinnerNavigationItems.get(position).getTitle());

        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {

        if(null == convertView) {

            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            convertView = layoutInflater.inflate(R.layout.list_item_title_navigation, null);
        }

        imageIcon = (ImageView) convertView.findViewById(R.id.imageIcon);

        textTitle = (TextView) convertView.findViewById(R.id.textTitle);

        imageIcon.setImageResource(spinnerNavigationItems.get(position).getIcon());

        textTitle.setText(spinnerNavigationItems.get(position).getTitle());

        return convertView;
    }
}
