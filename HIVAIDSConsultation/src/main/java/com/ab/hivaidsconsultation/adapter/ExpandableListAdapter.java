package com.ab.hivaidsconsultation.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.ab.hivaidsconsultation.activity.R;
import com.ab.hivaidsconsultation.model.FAQ;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by biniam on 3/27/14.
 */
public class ExpandableListAdapter extends BaseExpandableListAdapter {

    LayoutInflater inflater;

    private List<FAQ> allFaqs, originalListOfFaqs;

    public ExpandableListAdapter(Context context, List<FAQ> allFaqs) {

        this.allFaqs = allFaqs;
	    this.originalListOfFaqs = allFaqs;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public Object getChild(int questionPosition, int answerPosition) {
        return allFaqs.get(questionPosition).getAnswer();
    }

    @Override
    public long getChildId(int questionPosition, int answerPosition) {
        return answerPosition;
    }

    @Override
    public View getChildView(int questionPosition, int answerPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        String answer = allFaqs.get(questionPosition).getAnswer();

        TextView textAnswer =  null;

        if(null == convertView) {

            convertView = inflater.inflate(R.layout.faq_answer, null);
        }

        textAnswer = (TextView) convertView.findViewById(R.id.answer);
        textAnswer.setText(answer);

        return convertView;
    }

    @Override
    public int getChildrenCount(int questionPosition) {
        return 1;
        //return faqs.get(questionPosition).getAnswer().size();
    }

    @Override
    public Object getGroup(int questionPosition) {
        return allFaqs.get(questionPosition).getQuestion();
    }

    @Override
    public int getGroupCount() {
        return allFaqs.size();
    }

    @Override
    public long getGroupId(int questionPosition) {
        return questionPosition;
    }

    @Override
    public View getGroupView(int questionPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        String question = allFaqs.get(questionPosition).getQuestion();

        TextView textQuestion = null;

        if(null == convertView) {

            convertView = inflater.inflate(R.layout.faq_question, null);
        }

        textQuestion = (TextView) convertView.findViewById(R.id.question);
        textQuestion.setText(question);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int questionPosition, int answerPosition) {
        return false;
    }

    public void searchFAQ(String query){

        query = query.toLowerCase();
		allFaqs.clear();

        if(query.isEmpty()){

            allFaqs.addAll(originalListOfFaqs);

        } else {

            for(FAQ faq: allFaqs){

				if(faq.getQuestion().toLowerCase().contains(query) || faq.getAnswer().toLowerCase().contains(query)) {
                        allFaqs.add(faq);
				}
			}
		}

        notifyDataSetChanged();
    }

}
