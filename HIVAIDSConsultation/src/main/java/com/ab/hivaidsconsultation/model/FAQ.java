package com.ab.hivaidsconsultation.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by biniam on 3/27/14.
 */
public class FAQ {

    private String question;
    private String answer;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() { return answer; }

    public void setAnswer(String answer) { this.answer = answer; }

}
