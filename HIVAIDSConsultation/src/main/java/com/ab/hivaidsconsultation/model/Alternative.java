package com.ab.hivaidsconsultation.model;

/**
 * Created by Biniam on 3/27/14.
 */
public class Alternative {

	private String alternative_name;
	private String case_true;

	public Alternative() { }

	public Alternative(String alternative_name, String case_true) {
		this.alternative_name = alternative_name;
		this.case_true = case_true;
	}

	public String getAlternative_name() {
		return alternative_name;
	}

	public void setAlternative_name(String alternative_name) {
		this.alternative_name = alternative_name;
	}

	public String getCase_true() {
		return case_true;
	}

	public void setCase_true(String case_true) {
		this.case_true = case_true;
	}

}
