package com.ab.hivaidsconsultation.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Biniam on 3/27/14.
 */
public class Consultation {

	private String number;
	private String question;
	private String description;
	private List<Alternative> alternatives;

	public Consultation() {	}

	public Consultation(String number) {
		this.number = number;
	}

	public String getNumber() { return number; }

	public void setNumber(String number) { this.number = number; }

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Alternative> getAlternatives() {
		return alternatives;
	}

	public void setAlternatives(List<Alternative> alternatives) {
		this.alternatives = alternatives;
	}

	public void addAlternative(String alternative_name, String case_true) {

		Alternative alternative = new Alternative(alternative_name, case_true);

		this.alternatives.add(alternative);
	}

	public void addAlternative(Alternative alternative) {

        if(null == this.alternatives) {

            this.alternatives = new ArrayList<Alternative>();
            this.alternatives.add(alternative);

        } else {

            this.alternatives.add(alternative);
        }

	}

	/*
	protected static class Alternative {

		private String alternative_name;
		private String case_true;

		public Alternative(String alternative_name, String case_true) {
			this.alternative_name = alternative_name;
			this.case_true = case_true;
		}

		public String getAlternative_name() {
			return alternative_name;
		}

		public void setAlternative_name(String alternative_name) {
			this.alternative_name = alternative_name;
		}

		public String getCase_true() {
			return case_true;
		}

		public void setCase_true(String case_true) {
			this.case_true = case_true;
		}
	}
	*/
}


