package com.ab.hivaidsconsultation.model;

/**
 * Created by biniam on 3/26/14.
 */
public class SpinnerNavigationItem {

    private int title;

    private int icon;

    public SpinnerNavigationItem(int title, int icon) {
        this.title = title;
        this.icon = icon;
    }

    public int getTitle() {
        return title;
    }

    public int getIcon() {
        return icon;
    }
}
