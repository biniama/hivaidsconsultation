package com.ab.hivaidsconsultation.model;

/**
 * Created by biniam on 4/23/14.
 */
public class Answer {

    private String answer;

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
