package com.ab.hivaidsconsultation.general;

/**
 * Created by biniam on 3/26/14.
 */
public class Constants {

    public static final int FUNCTIONALITY_HOME_ID = 0;
    public static final int FUNCTIONALITY_CONSULTATION_ID = 1;
    public static final int FUNCTIONALITY_FAQ_ID = 2;
    public static final int FUNCTIONALITY_LOCATE_VCT_ID = 3;

    /*public static final String BUTTON_NEXT = "Next";
    public static final String BUTTON_PREVIOUS = "Previous";*/

    public static final String QUESTION_PREFIX = "question_";
    public static final String RECOMMENDATION_MESSAGE = "recommendationMessage";

    //public static final String FAQ_FILE_NAME = "faqs.xml";

    public static final String FONT_NYALA_FILE_LOCATION = "fonts/nyala.ttf";

    // Locales
    public static final String ACTIVITY_PROPERTY_LOCALE_ENGLISH = "en";
    public static final String ACTIVITY_PROPERTY_LOCALE_AMHARIC = "am";
    public static final String ACTIVITY_PROPERTY_LOCALE_OROMIFFA = "om";


}
