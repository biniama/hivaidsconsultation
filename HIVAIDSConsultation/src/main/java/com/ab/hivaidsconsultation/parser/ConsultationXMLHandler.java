package com.ab.hivaidsconsultation.parser;

import com.ab.hivaidsconsultation.model.Alternative;
import com.ab.hivaidsconsultation.model.Consultation;
import com.ab.hivaidsconsultation.model.FAQ;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by biniam on 3/27/14.
 */
public class ConsultationXMLHandler extends DefaultHandler {

    private List<Consultation> consultations;
    private String tempVal;
    private Consultation tempConsultation;
	private Alternative tempAlternative;

    public ConsultationXMLHandler() {

	    consultations = new ArrayList<Consultation>();
    }

    public List<Consultation> getConsultations() {
        return consultations;
    }

    // Event Handlers
    public void startElement(String uri, String localName, String qName,
                             Attributes attributes) throws SAXException {
        // reset
        tempVal = "";
        if (qName.equalsIgnoreCase("consultation")) {
            // create a new instance of Consultation
	        tempConsultation = new Consultation();
        }

        if (qName.equalsIgnoreCase("alternative")) {
            // create a new instance of Alternative
            tempAlternative = new Alternative();
        }
    }

    public void characters(char[] ch, int start, int length) throws SAXException {

        tempVal = new String(ch, start, length);
    }

    public void endElement(String uri, String localName, String qName) throws SAXException {

        if (qName.equalsIgnoreCase("consultation")) {
            // add it to the list
	        consultations.add(tempConsultation);

        } else if (qName.equalsIgnoreCase("number")) {
	        tempConsultation.setNumber(tempVal);

        } else if (qName.equalsIgnoreCase("question")) {
            tempConsultation.setQuestion(tempVal);

        } else if (qName.equalsIgnoreCase("description")) {
	        tempConsultation.setDescription(tempVal);

        } else if (qName.equalsIgnoreCase("alternative_name")) {
			tempAlternative.setAlternative_name(tempVal);

		} else if (qName.equalsIgnoreCase("case_true")) {
			tempAlternative.setCase_true(tempVal);
	        tempConsultation.addAlternative(tempAlternative);
		}
	}
}
