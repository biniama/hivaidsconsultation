package com.ab.hivaidsconsultation.parser;

import android.util.Log;

import com.ab.hivaidsconsultation.model.Consultation;
import com.ab.hivaidsconsultation.model.FAQ;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import java.io.InputStream;
import java.util.List;

import javax.xml.parsers.SAXParserFactory;

/**
 * Created by biniam on 3/27/14.
 */
public class XMLParser {

    public static List<FAQ> parseFAQ(InputStream is) {

        List<FAQ> faqs = null;

        try {
            // create a XMLReader from SAXParser
            XMLReader xmlReader = SAXParserFactory.newInstance().newSAXParser().getXMLReader();
            // create a FaqXMLHandler
            FaqXMLHandler faqXMLHandler = new FaqXMLHandler();
            // store handler in XMLReader
            xmlReader.setContentHandler(faqXMLHandler);
            // the process starts
            xmlReader.parse(new InputSource(is));
            // get the `FAQ list`
            faqs = faqXMLHandler.getFaqs();

        } catch (Exception ex) {
            Log.e("Exception in parseFAQ", ex.getMessage());
        }

        // return FAQ list
        return faqs;
    }

	public static List<Consultation> parseConsultation(InputStream is) {

		List<Consultation> consultations = null;

		try {
			// create a XMLReader from SAXParser
			XMLReader xmlReader = SAXParserFactory.newInstance().newSAXParser().getXMLReader();
			// create a ConsultationXMLHandler
			ConsultationXMLHandler consultationXMLHandler = new ConsultationXMLHandler();
			// store handler in XMLReader
			xmlReader.setContentHandler(consultationXMLHandler);
			// the process starts
			xmlReader.parse(new InputSource(is));
			// get the `Consultations list`
			consultations = consultationXMLHandler.getConsultations();

		} catch (Exception ex) {
			Log.e("Exception in parseConsultation", ex.getMessage());
		}

		// return Consultations list
		return consultations;
	}
}
