package com.ab.hivaidsconsultation.parser;

import com.ab.hivaidsconsultation.model.FAQ;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by biniam on 3/27/14.
 */
public class FaqXMLHandler extends DefaultHandler {

    private List<FAQ> faqs;
    private String tempVal;
    private FAQ tempFaq;

    public FaqXMLHandler() {

        faqs = new ArrayList<FAQ>();
    }

    public List<FAQ> getFaqs() {
        return faqs;
    }

    // Event Handlers
    public void startElement(String uri, String localName, String qName,
                             Attributes attributes) throws SAXException {
        // reset
        tempVal = "";
        if (qName.equalsIgnoreCase("faq")) {
            // create a new instance of employee
            tempFaq = new FAQ();
        }
    }

    public void characters(char[] ch, int start, int length) throws SAXException {

        tempVal = new String(ch, start, length);
    }

    public void endElement(String uri, String localName, String qName) throws SAXException {

        if (qName.equalsIgnoreCase("faq")) {
            // add it to the list
            faqs.add(tempFaq);

        } else if (qName.equalsIgnoreCase("question")) {
            tempFaq.setQuestion(tempVal);

        } else if (qName.equalsIgnoreCase("answer")) {
            tempFaq.setAnswer(tempVal);
        }
    }

}
