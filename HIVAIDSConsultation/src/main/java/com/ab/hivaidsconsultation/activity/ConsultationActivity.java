package com.ab.hivaidsconsultation.activity;

import android.os.Bundle;
import androidx.appcompat.app.ActionBar;

/**
 * Created by biniam on 4/7/14.
 */
public class ConsultationActivity extends AbstractActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.consultation_activity);

        ActionBar actionBar = getSupportActionBar();

        // Enabling Up / Back navigation
        actionBar.setDisplayHomeAsUpEnabled(true);

        actionBar.setTitle(R.string.title_consultation);

        // Check whether the activity is using the layout version with
        // the fragment_container FrameLayout. If so, we must add the first fragment
        if (findViewById(R.id.fragment_container) != null) {

            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }

            // Create an instance of ConsultationFragment
            ConsultationFragment consultation = new ConsultationFragment();

            // In case this activity was started with special instructions from an Intent,
            // pass the Intent's extras to the fragment as arguments
            consultation.setArguments(getIntent().getExtras());

            // Add the fragment to the 'fragment_container'
            getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, consultation).commit();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
