package com.ab.hivaidsconsultation.activity;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;

import com.ab.hivaidsconsultation.general.Constants;

/**
 * Created by biniam on 4/8/14.
 */

public class HomeActivity extends AbstractActionBarActivity implements ActionBar.OnNavigationListener {

    /**
     * The serialization (saved instance state) Bundle key representing the
     * current dropdown position.
     */
    private static final String STATE_SELECTED_NAVIGATION_ITEM = "selected_navigation_item";

    // action bar
    private ActionBar actionBar;

    String[] dropdownValues;

    private boolean navigationCalledFirstTime = true;
    // Title navigation Spinner data
    //private ArrayList<SpinnerNavigationItem> navSpinner;

    // Navigation adapter
    //private TitleNavigationAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // Set up the action bar to show a dropdown list.
        actionBar = getSupportActionBar();

        // Hide the action bar title
        actionBar.setDisplayShowTitleEnabled(false);

        // Enabling Spinner dropdown navigation
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);

        dropdownValues = getResources().getStringArray(R.array.main_functionalities_menu);

        // Specify a SpinnerAdapter to populate the dropdown list.
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(actionBar.getThemedContext(),
                android.R.layout.simple_list_item_1, android.R.id.text1, dropdownValues);

        // Set up the dropdown list navigation in the action bar.
        actionBar.setListNavigationCallbacks(adapter, this);

        // Override calling onNavigationItem for the first item
        actionBar.setSelectedNavigationItem(0);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();

        menuInflater.inflate(R.menu.main, menu);

        return super.onCreateOptionsMenu(menu);
    }

    /**
     * On selecting action bar icons
     * */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        // Take appropriate action for each action item click
        switch (item.getItemId()) {

            case R.id.action_help:
                // Go to help activity
                // Toast.makeText(this, "Help is selected", Toast.LENGTH_SHORT).show();

                goToHelp(HomeActivity.this);
                break;

            case R.id.action_about:

                goToAbout(HomeActivity.this);
                break;

            case R.id.action_language_english:
                // English language
                Toast.makeText(this, "English language is selected", Toast.LENGTH_SHORT).show();

                changeLocale(Constants.ACTIVITY_PROPERTY_LOCALE_ENGLISH);
                // Reload the home activity with changed language
                goToHome(HomeActivity.this);
                break;

            case R.id.action_language_amharic:
                // Amharic language
                Toast.makeText(this, "Amharic language is selected", Toast.LENGTH_SHORT).show();

                changeLocale(Constants.ACTIVITY_PROPERTY_LOCALE_AMHARIC);
                // Reload the home activity with changed language
                goToHome(HomeActivity.this);
                break;

            case R.id.action_language_oromiffa:
                // Oromiffa language
                Toast.makeText(this, "Oromiffa language is selected", Toast.LENGTH_SHORT).show();

                changeLocale(Constants.ACTIVITY_PROPERTY_LOCALE_OROMIFFA);
                // Reload the home activity with changed language
                goToHome(HomeActivity.this);
                break;

            default:
                return super.onOptionsItemSelected(item);
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Actionbar navigation item select listener
     * */
    @Override
    public boolean onNavigationItemSelected(int itemPosition, long itemId) {

        switch (itemPosition){

            case Constants.FUNCTIONALITY_HOME_ID:

                // Reload the home activity
                reloadHome();
                break;

            case Constants.FUNCTIONALITY_CONSULTATION_ID:

                goToConsultation(HomeActivity.this);
                break;

            case Constants.FUNCTIONALITY_FAQ_ID:

                goToFAQ(HomeActivity.this);
                break;

            case Constants.FUNCTIONALITY_LOCATE_VCT_ID:
                goToLocateVCT(HomeActivity.this);
                break;
        }

        return false;
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore the previously serialized current dropdown position.
        if (savedInstanceState.containsKey(STATE_SELECTED_NAVIGATION_ITEM)) {
            getSupportActionBar().setSelectedNavigationItem(savedInstanceState.getInt(STATE_SELECTED_NAVIGATION_ITEM));
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        // Serialize the current dropdown position.
        outState.putInt(STATE_SELECTED_NAVIGATION_ITEM, getSupportActionBar().getSelectedNavigationIndex());
    }

    private void reloadHome() {

        setContentView(R.layout.home_activity);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
