package com.ab.hivaidsconsultation.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

/**
 * Created by biniam on 4/23/14.
 */
public class AbstractActivity extends Activity {

    public void goToHome(Context context) {

        Intent intent = new Intent(context, HomeActivity.class);
        startActivity(intent);

        // close the activity
        finish();
    }

    public void goToLocateVCT(Context context) {

        Intent intent = new Intent(context, LocateVCTActivity.class);
        startActivity(intent);

        // close the activity
        finish();
    }

}
