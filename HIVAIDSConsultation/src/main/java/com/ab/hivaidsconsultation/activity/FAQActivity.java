package com.ab.hivaidsconsultation.activity;

import android.os.Bundle;
import android.util.Log;
import android.widget.ExpandableListView;

import androidx.appcompat.app.ActionBar;

import com.ab.hivaidsconsultation.adapter.ExpandableListAdapter;
import com.ab.hivaidsconsultation.model.FAQ;
import com.ab.hivaidsconsultation.parser.XMLParser;

import java.util.List;

/**
 * Created by biniam on 3/26/14.
 */
public class FAQActivity extends AbstractActionBarActivity implements ExpandableListView.OnGroupExpandListener {

	private ExpandableListView faqExpandableListView;

	private ExpandableListAdapter expandableListAdapter;

	private List<FAQ> allFaqs;

    private int lastExpandedGroupPosition = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.faq_activity);

        // get the action bar
        ActionBar actionBar = getSupportActionBar();

        // Enabling Up / Back navigation
        actionBar.setDisplayHomeAsUpEnabled(true);

        try {

            allFaqs = XMLParser.parseFAQ(getResources().openRawResource(R.raw.faqs));

            faqExpandableListView = (ExpandableListView) findViewById(R.id.faq_list);

            expandableListAdapter = new ExpandableListAdapter(FAQActivity.this, allFaqs);

            faqExpandableListView.setAdapter(expandableListAdapter);

            faqExpandableListView.setOnGroupExpandListener(this);

        } catch (Exception ex) {

            Log.e("Exception", ex.getMessage());
        }
    }

    @Override
    public void onGroupExpand(int groupPosition) {

        if (lastExpandedGroupPosition != -1
                && groupPosition != lastExpandedGroupPosition) {

            faqExpandableListView.collapseGroup(lastExpandedGroupPosition);
        }
        lastExpandedGroupPosition = groupPosition;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}