package com.ab.hivaidsconsultation.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.ab.hivaidsconsultation.activity.R;
import com.ab.hivaidsconsultation.general.Constants;

/**
 * Created by biniam on 4/4/14.
 */
public class RecommendationActivity extends AbstractActivity implements View.OnClickListener {

    Button goToHomeButton, goToLocateVctButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.recommendation_activity);

        setTitle(R.string.text_recommendation_header);

        String recommendationMessage = getIntent().getExtras().getString(Constants.RECOMMENDATION_MESSAGE);

        ((TextView) findViewById(R.id.textRecommendationMessage)).setText(recommendationMessage);

        goToHomeButton = (Button) findViewById(R.id.button_go_to_home);
        goToHomeButton.setOnClickListener(this);

        goToLocateVctButton = (Button) findViewById(R.id.button_go_to_locate_vct);
        goToLocateVctButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.button_go_to_home:

                goToHome(RecommendationActivity.this);
                break;

            case R.id.button_go_to_locate_vct:

                goToLocateVCT(RecommendationActivity.this);
                finish();
                break;
        }

    }

}
