package com.ab.hivaidsconsultation.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Handler;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.ab.hivaidsconsultation.general.Constants;

import java.util.Locale;

/**
 * Created by biniam on 4/8/14.
 */

public class AbstractActionBarActivity extends AppCompatActivity {

    private Boolean doubleBackToExitPressedOnce = false;

    protected void changeLocale(final String selectedLocale) {

        Locale locale = new Locale(selectedLocale);

        Locale.setDefault(locale);

        Configuration config = new Configuration();

        config.locale = locale;

        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }

    protected Locale getLocale() {

        return getBaseContext().getResources().getConfiguration().locale;
    }

    @Override
    public void onBackPressed() {

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();

            // Reset the Locale of the phone to English
            changeLocale(Constants.ACTIVITY_PROPERTY_LOCALE_ENGLISH);

            return;
        }

        this.doubleBackToExitPressedOnce = true;

        Toast.makeText(this, R.string.text_click_back_again_to_exit, Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }

        }, 2000);
    }

    public void goToHome(Context context) {

        Intent intent = new Intent(context, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    public void goToConsultation(Context context) {

        Intent intent = new Intent(context, ConsultationActivity.class);
        startActivity(intent);
        //finish();
    }

    public void goToFAQ(Context context) {

        Intent intent = new Intent(context, FAQActivity.class);
        startActivity(intent);
        //finish();
    }

    public void goToHelp(Context context) {

        Intent intent = new Intent(context, HelpActivity.class);
        startActivity(intent);
        //finish();
    }

    public void goToAbout(Context context) {

        Intent intent = new Intent(context, AboutActivity.class);
        startActivity(intent);
        //finish();
    }

    public void goToLocateVCT(Context context) {

        Intent intent = new Intent(context, LocateVCTActivity.class);
        startActivity(intent);
        //finish();
    }
}
