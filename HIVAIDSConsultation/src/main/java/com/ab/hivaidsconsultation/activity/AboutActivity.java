package com.ab.hivaidsconsultation.activity;

import android.os.Bundle;
import androidx.appcompat.app.ActionBar;

/**
 * Created by biniam on 4/23/14.
 */
public class AboutActivity extends AbstractActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.about_activity);

        // get the action bar
        ActionBar actionBar = getSupportActionBar();

        // Enabling Up / Back navigation
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
