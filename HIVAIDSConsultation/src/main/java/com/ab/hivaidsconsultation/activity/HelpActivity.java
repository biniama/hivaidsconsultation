package com.ab.hivaidsconsultation.activity;

import android.os.Bundle;
import androidx.appcompat.app.ActionBar;

/**
 * Created by Biniam on 3/26/14.
 */
public class HelpActivity extends AbstractActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.help_activity);

        // get the action bar
        ActionBar actionBar = getSupportActionBar();

        // Enabling Up / Back navigation
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
