package com.ab.hivaidsconsultation.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

/**
 * Created by biniam on 4/8/14.
 */
public class SplashScreenActivity extends AbstractActivity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen_activity);

        new Handler().postDelayed(new Runnable() {

            // Showing splash screen with a timer.

            @Override
            public void run() {

                // This method will be executed once the timer is over

                // Start the home activity
                goToHome(SplashScreenActivity.this);

            }

        }, SPLASH_TIME_OUT);
    }
}
