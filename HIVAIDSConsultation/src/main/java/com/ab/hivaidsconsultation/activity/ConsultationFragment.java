package com.ab.hivaidsconsultation.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.ab.hivaidsconsultation.general.Constants;
import com.ab.hivaidsconsultation.model.Alternative;
import com.ab.hivaidsconsultation.model.Consultation;
import com.ab.hivaidsconsultation.parser.XMLParser;

import java.util.List;

/**
 * Created by biniam on 3/14/14.
 */
public class ConsultationFragment extends Fragment implements RadioGroup.OnCheckedChangeListener {  // View.OnClickListener,

    /**
     * A placeholder fragment containing a simple view.
     */

     /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    private static final String QUESTION_ONE_NUMBER = "question_one";

    private List<Consultation> allConsultations;

    private int currentQuestionId;

    private TextView textViewQuestion, textViewDescription;

    private RadioButton radioButtonOne, radioButtonTwo, radioButtonThree, radioButtonFour, radioButtonFive;

    private RadioGroup radioGroup;

    private String [] consultationItem = null;

	private Consultation nextConsultation;

    /**
     * Returns a new instance of this fragment for the given section number.
     */
    public static ConsultationFragment newInstance(int sectionNumber) {

        ConsultationFragment fragment = new ConsultationFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.consultation_fragment, container, false);

        initializeViewElements(rootView);

	    loadConsultationsFromXML();

        loadQuestion(QUESTION_ONE_NUMBER);

        return rootView;
    }

    private void initializeViewElements(View rootView) {

        textViewQuestion = (TextView) rootView.findViewById(R.id.textViewQuestion);
        textViewDescription = (TextView) rootView.findViewById(R.id.textViewDescription);

        radioButtonOne = (RadioButton) rootView.findViewById(R.id.radioButtonOne);
        radioButtonTwo = (RadioButton) rootView.findViewById(R.id.radioButtonTwo);
        radioButtonThree = (RadioButton) rootView.findViewById(R.id.radioButtonThree);
        radioButtonFour = (RadioButton) rootView.findViewById(R.id.radioButtonFour);
        radioButtonFive = (RadioButton) rootView.findViewById(R.id.radioButtonFive);

        radioGroup = (RadioGroup) rootView.findViewById(R.id.radioGroup);
        radioGroup.setOnCheckedChangeListener(this);

    }

    public void loadConsultationsFromXML() {

        allConsultations = XMLParser.parseConsultation(getActivity().getResources().openRawResource(R.raw.consultations));
    }

    private void loadQuestion(String questionNumber) {

        clearSelectedRadioButtons();

        //setTypeFaceForEthiopicScripts();

        for(Consultation consultation: allConsultations){

            if(consultation.getNumber().equals(questionNumber)) {

                textViewQuestion.setText(consultation.getQuestion());

                if(consultation.getDescription().trim().isEmpty())
                    textViewDescription.setVisibility(View.GONE);
                else
                    textViewDescription.setText(consultation.getDescription());

                Integer numberOfAlternatives = consultation.getAlternatives().size();

                if(numberOfAlternatives == 0) {

                    //throw new Exception("No alternatives");
                    Toast.makeText(getActivity(), "No alternatives", Toast.LENGTH_LONG).show();

                } else if (numberOfAlternatives == 1) {

                    //throw new Exception("Only one alternative");
                    Toast.makeText(getActivity(), "Only one alternative", Toast.LENGTH_LONG).show();

                } else if (numberOfAlternatives == 2) {

                    radioButtonOne.setText(consultation.getAlternatives().get(0).getAlternative_name());
                    radioButtonTwo.setText(consultation.getAlternatives().get(1).getAlternative_name());
                    radioButtonThree.setVisibility(View.GONE);
                    radioButtonFour.setVisibility(View.GONE);
                    radioButtonFive.setVisibility(View.GONE);

                } else if (numberOfAlternatives == 3) {

                    radioButtonOne.setText(consultation.getAlternatives().get(0).getAlternative_name());
                    radioButtonTwo.setText(consultation.getAlternatives().get(1).getAlternative_name());
                    radioButtonThree.setText(consultation.getAlternatives().get(2).getAlternative_name());
                    radioButtonFour.setVisibility(View.GONE);
                    radioButtonFive.setVisibility(View.GONE);

                } else if (numberOfAlternatives == 4) {

                    radioButtonOne.setText(consultation.getAlternatives().get(0).getAlternative_name());
                    radioButtonTwo.setText(consultation.getAlternatives().get(1).getAlternative_name());
                    radioButtonThree.setText(consultation.getAlternatives().get(2).getAlternative_name());
				    radioButtonFour.setText(consultation.getAlternatives().get(3).getAlternative_name());
                    radioButtonFive.setVisibility(View.GONE);

                } else if (numberOfAlternatives == 5) {

                    radioButtonOne.setText(consultation.getAlternatives().get(0).getAlternative_name());
                    radioButtonTwo.setText(consultation.getAlternatives().get(1).getAlternative_name());
                    radioButtonThree.setText(consultation.getAlternatives().get(2).getAlternative_name());
                    radioButtonFour.setText(consultation.getAlternatives().get(3).getAlternative_name());
                    radioButtonFive.setText(consultation.getAlternatives().get(4).getAlternative_name());
                }
            }
        }
    }

    /*
    private void setTypeFaceForEthiopicScripts() {

        if( Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {

            Locale locale = getResources().getConfiguration().locale;

            if(locale.getLanguage().equals(Constants.ACTIVITY_PROPERTY_LOCALE_AMHARIC)){

                Typeface TYPE_FACE_NYALA = Typeface.createFromAsset(getResources().getAssets(), Constants.FONT_NYALA_FILE_LOCATION);

                textViewQuestion.setTypeface(TYPE_FACE_NYALA);
                textViewDescription.setTypeface(TYPE_FACE_NYALA);

                radioButtonOne.setTypeface(TYPE_FACE_NYALA);
                radioButtonTwo.setTypeface(TYPE_FACE_NYALA);
                radioButtonThree.setTypeface(TYPE_FACE_NYALA);
                radioButtonFour.setTypeface(TYPE_FACE_NYALA);
                radioButtonFive.setTypeface(TYPE_FACE_NYALA);
            }
        }
    }
    */

    private void clearSelectedRadioButtons() {

        radioButtonOne.setChecked(false);
        radioButtonTwo.setChecked(false);
        radioButtonThree.setChecked(false);
        radioButtonFour.setChecked(false);
        radioButtonFive.setChecked(false);
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int checkedRadioButtonId) {

        String currentQuestion = null;
        String nextAction = null;

        for(Consultation consultation : allConsultations) {

            if(consultation.getQuestion().equals(textViewQuestion.getText())) {

                currentQuestion = consultation.getNumber();

                for(Alternative alternative : consultation.getAlternatives()) {

                    if(alternative.getAlternative_name().equals(((RadioButton) getActivity().findViewById(checkedRadioButtonId)).getText())) {

                        nextAction = alternative.getCase_true();

                        if(nextAction.startsWith(Constants.QUESTION_PREFIX)) {

                            loadQuestion(nextAction);
                            return;

                        } else {

                            goToRecommendation(nextAction);
                            return;
                        }
                    }
                }
            }
        }
    }

    private void goToRecommendation(String recommendationMessage) {

        Intent intent = new Intent(getActivity(), RecommendationActivity.class);
        intent.putExtra(Constants.RECOMMENDATION_MESSAGE, recommendationMessage);

        // end this fragments activity
        getActivity().finish();

        startActivity(intent);
    }
}
